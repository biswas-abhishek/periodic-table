// -------------- Defining Variable -------------

var trans = "rgb(221, 219, 215)";
var SBlockColor = "#ff6464";
var PBlockColor = "#fbfd5b";
var DBlockColor = "#62b1ff";
var FBlockColor = "#5ffd5c";
const Up_Btn = document.getElementById("up-btn");
const Down_Btn = document.getElementById("down-btn");
const Bundle_1 = document.getElementById("bundle__1");
const bundle_2 = document.getElementById("bundle__2");
const FBlock_Pt_1 = document.getElementById('block-f-1');
const FBlock_Pt_2 = document.getElementById('block-f-2');
const hover_show_ele_1 = document.getElementById('element-16');
const hover_show_ele_2 = document.getElementById('element-17');
Up_Btn.addEventListener("click", () => {
  bundle_2.style.display = "none";
  Bundle_1.style.display = "block";
});
Down_Btn.addEventListener("click", () => {
  bundle_2.style.display = "flex";
  Bundle_1.style.display = "none";
});

hover_show_ele_1.addEventListener('click',()=>{
  if (FBlock_Pt_1.style.display === 'block') {
    FBlock_Pt_1.style.display = 'none';
  } else {
    FBlock_Pt_1.style.display = 'block';
  }
});
hover_show_ele_2.addEventListener('click',()=>{
  if (FBlock_Pt_2.style.display === 'block') {
    FBlock_Pt_2.style.display = 'none';
  } else {
    FBlock_Pt_2.style.display = 'block';
  }
});